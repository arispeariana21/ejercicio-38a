import { Component, OnInit } from '@angular/core';
import { Validacion } from 'src/app/interface/validacion';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  datos: Validacion = {
    nombre:'',
    direccion:'',
  }

}
